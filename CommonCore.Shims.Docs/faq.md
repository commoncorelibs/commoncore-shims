﻿# Frequently Asked Questions

## Do I need this library?

Almost certainly not.

## Who does need this library?

Pretty much only projects that have to target net standard 2.0 compliant pre-core versions of dotnet (such as .Net Framework) or libraries that need to work with such projects. And even then only if they want to use C# 9 features like slicing and init properties.

## How does this library help?

It provides the right classes in the right namespaces to allow the compiler to use the new features under the legacy target. Basically the same thing dotnet does naturally in net standard 2.1 and higher.
