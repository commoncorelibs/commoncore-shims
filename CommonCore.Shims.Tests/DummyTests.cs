using System;
using FluentAssertions;
using Xunit;

namespace CommonCore.Shims.Tests
{
    public class DummyTests
    {
        [Fact]
        public void Test()
        {
            int[] subject = new int[] { 9, 5, 1 };
            int[] expected;

            expected = new int[] { 5, 1 };
            subject[1..].Should().BeEquivalentTo(expected);

            expected = new int[] { 9, 5 };
            subject[..^1].Should().BeEquivalentTo(expected);

            expected = new int[] { 5 };
            subject[1..^1].Should().BeEquivalentTo(expected);
        }
    }
}
