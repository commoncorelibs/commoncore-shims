# Release v1.0.0-pre.2

* Library now targets both net standard 2.0 and 2.1 [#1](https://gitlab.com/commoncorelibs/commoncore-shims/-/issues/1)
* Fix init properties unavailable in dotnet standard 2.1 [#1](https://gitlab.com/commoncorelibs/commoncore-shims/-/issues/1)
