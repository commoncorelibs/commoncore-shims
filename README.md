# Vinland Solutions CommonCore.Shims Library

[![License](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/license/mit.svg)](https://opensource.org/licenses/MIT)
[![Platform](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/platform/netstandard/2.0_2.1.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-shims)
[![Releases](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-shims/-/releases)
[![Documentation](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-shims/)  
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.Shims/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Shims/)
[![Pipeline](https://gitlab.com/commoncorelibs/commoncore-shims/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-shims/commits/master)
[![Coverage](https://gitlab.com/commoncorelibs/commoncore-shims/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-shims/reports/tests.html)

**NOTE: This project should only be used by legacy pre-dotnet core projects and libraries that multi-target net standard 2.0 to reach such projects.**

**CommonCore.Shims** is a .Net Standard 2.0/2.1 library that provides shims for C# 9 features like collection slicing and initializable properties.

## Installation

The official release versions of the **CommonCore.Shims** library are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Shims/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

This library is compatible with projects targeting at least .Net Framework 4.6.1, .Net Core 2.0, or .Net 5.

## Usage

This library does not have any special usage of it's own. It simply enables C# language features that are usually present but unusable in projects targeting net standard 2.1 or lower. It may be necessary to explicitly enable C# 9 language features, in which case a `LangVersion` element must be added to the project file.

```xml
    <PropertyGroup>
        <TargetFrameworks>netstandard2.1;netstandard2.0</TargetFrameworks>
        <LangVersion>9.0</LangVersion>
    </PropertyGroup>
```

Otherwise, most shimmed features work without any additional effort.

Externally initializable properties:
```csharp
public class SomeClass
{
    public int InitProp { get; init; } = 10;
}

var sc = new SomeClass();
Console.WriteLine(sc.InitProp);// InitProp == 10
sc = new SomeClass() { InitProp = 5 };
Console.WriteLine(sc.InitProp);// InitProp == 5
sc.InitProp = 0;// Error
```

Some code analysis attributes:
```csharp
public static bool TryParse(
    [NotNullWhen(true)] object? input,
    [NotNullWhen(true), MaybeNullWhen(false)] out SomeType? output)
{
    // if input is not null and can parse input,
    // then set output to result and return true,
    // else set output null and return false.
}
```

However, there is one more additional step required to convince the compiler that collection slicing is enabled. C# language features, like slicing, seem fancy, but they boil down to calling methods just like any other code. In this case, slicing calls the `GetSubArray()` method on the `RuntimeHelpers` class in the `System.Runtime.CompilerServices`.

Without that method, slicing won't work. Unfortunately, unlike most shimming solutions, the library can't simply declare the class and implement the method to solve this one. The reason is that the class already exists, but the net standard 2.0 version of the class doesn't have the neeeded method. If the library declared the class with the method and made public so other libraries could have it for their compiler, it would still be an error because the compiler sees two versions of the same class. However, there is a loophole.

The problem isn't the compiler seeing two versions of the same class. The problem is the compiler seeing to **public** versions of the same class. If the class is `internal`, then the problem goes away. The compiler can find the needed method on the class and isn't confused because of public ambiguity. But if the class is in the library and internal, then that doesn't help anyone other than me writing this library. The solution is straight forward at this point.

Consuming projects must define the class themselves and it has to be internal. The library does wrap the implementation of the `GetSubArray()`, so it can wrapped to avoid too much bloat. It is highly suggested to put the class in a file named `__RuntimeHelpers.cs`, note the two leading underscores, or nest it in a `System/Runtime/CompilerServices/RuntimeHelpers.cs` folder hierarchy, or in some other way make it very apparent that the code isn't a normal part of the library.

THe code is wrapped in an if-directive so it is only included in the compilation for the needed frameworks. A short comment is given to the class so anyone viewing the file can have an idea why it exists. The `inheritdoc` tag is used to attach more detailed information to the method itself from it's wrapped counter part. Lastly, the `ExcludeFromCodeCoverage` and `EditorBrowsable` attributes are used to hide the class from code coverage stats and intellisence code completion, respectively.

```csharp
#if !NETSTANDARD2_1_OR_GREATER

using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace System.Runtime.CompilerServices
{
    /// <summary>
    /// Shim class to enable C# 9 slicing feature.
    /// Uses CommonCore.Shims for implementation.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal static class RuntimeHelpers
    {
        /// <inheritdoc cref="CommonCore.Shims.RuntimeHelpers.GetSubArray{T}(T[], Range)"/>
        public static T[] GetSubArray<T>(T[] array, Range range)
            => CommonCore.Shims.RuntimeHelpers.GetSubArray(array, range);
    }
}

#endif

```

Once that method is available to the compiler, collection slicing should work as one would expect.

Slicing collections:
```csharp
int[] array = new int[] { 9, 5, 1 };
int[] subarray;
subarray = array[1..];// { 5, 1 }
subarray = array[..^1];// { 9, 5 }
subarray = array[1..^1];// { 5 }
```

## Projects

The **CommonCore.Shims** repository is composed of three projects with the listed dependencies:

* **CommonCore.Shims**: The dotnet standard 2.0 library project.
  * [System.Memory](https://www.nuget.org/packages/System.Memory/)
  * [Microsoft.Bcl.HashCode](https://www.nuget.org/packages/Microsoft.Bcl.HashCode)
* **CommonCore.Shims.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **CommonCore.Shims.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-shims
- Issues: https://gitlab.com/commoncorelibs/commoncore-shims/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-shims/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.Shims/

## Credits

This library simply wraps some dotnet types that are built-in for net standard 2.1 and greater, so thanks goes to the dotnet team.

* [DotNet](https://github.com/dotnet)
