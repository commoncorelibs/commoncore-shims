﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
//
// https://github.com/dotnet/runtime/blob/fb931c6fa65470899f811f8102523cf320ee7a50/src/libraries/Common/src/System/Runtime/CompilerServices/IsExternalInit.cs
//
// NOTICE: Included to enable C# 9 property init feature despite the project targeting netstandard2.0.

#if NETSTANDARD2_1 || !NETSTANDARD2_1_OR_GREATER

using System.ComponentModel;

// ReSharper disable once CheckNamespace
namespace System.Runtime.CompilerServices
{
    /// <summary>
    /// Reserved to be used by the compiler for tracking metadata.
    /// This class should not be used by developers in source code.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class IsExternalInit { }
}

#endif
