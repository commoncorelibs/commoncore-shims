# Change Log

## 1.0.0-pre.2

* Library now targets both net standard 2.0 and 2.1 [#1](https://gitlab.com/commoncorelibs/commoncore-shims/-/issues/1)
* Fix init properties unavailable in dotnet standard 2.1 [#1](https://gitlab.com/commoncorelibs/commoncore-shims/-/issues/1)

## 1.0.0-pre.1

* Initial project creation.

### Slicing

* Add System.Index
* Add System.Range
* Add System.Runtime.CompilerServices.RuntimeHelpers

### Initializable Properties

* Add System.Runtime.CompilerServices.IsExternalInit

### Code Analysis Attributes

* Add System.Diagnostics.CodeAnalysis.AllowNullAttribute
* Add System.Diagnostics.CodeAnalysis.DisallowNullAttribute
* Add System.Diagnostics.CodeAnalysis.DoesNotReturnAttribute
* Add System.Diagnostics.CodeAnalysis.DoesNotReturnIfAttribute
* Add System.Diagnostics.CodeAnalysis.MaybeNullAttribute
* Add System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute
* Add System.Diagnostics.CodeAnalysis.MemberNotNullAttribute
* Add System.Diagnostics.CodeAnalysis.MemberNotNullWhenAttribute
* Add System.Diagnostics.CodeAnalysis.NotNullAttribute
* Add System.Diagnostics.CodeAnalysis.NotNullIfNotNullAttribute
* Add System.Diagnostics.CodeAnalysis.NotNullWhenAttribute
